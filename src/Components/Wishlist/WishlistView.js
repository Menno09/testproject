import React from "react";
import WishlistItemView from "./WishlistItemView"
import WishListitemAdd from "./WishListitemAdd";
import { observer } from "mobx-react";

const WishlistView = ({ list }) => {
    return(
        <div>
            <ul>
                {list.items.map((wishListItems, idx) =><WishlistItemView key={idx} items={wishListItems}/>)}
                <p>Total: {list.totalPrice}</p>
                <WishListitemAdd items={list}/>
            </ul>
        </div>
    )
}
export default observer(WishlistView)
