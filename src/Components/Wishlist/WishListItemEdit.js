import React from "react";
import { observer } from "mobx-react";

const WishListItemEdit = ({ items }) => {

    const onNameChange = event => {
        items.changeName(event.target.value)
    }

    const onPriceChange = event => {
        const price = parseInt(event.target.value)
        if (!isNaN(price))  items.changePrice(price)
    }

    const onImageChange = event => {
        items.changeImage(event.target.value)
    }

    return(
        <div>
            <input value={items.name} onChange={onNameChange}/>
            <input value={items.price} onChange={onPriceChange}/>
            <input value={items.image} onChange={onImageChange}/>
        </div>
    )

}

export default observer(WishListItemEdit)
