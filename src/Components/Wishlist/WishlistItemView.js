import React, {useState} from "react";
import WishListItemEdit from "./WishListItemEdit";
import { observer } from "mobx-react";
import { clone, getSnapshot, applySnapshot } from "mobx-state-tree"

const WishlistItemView = ({ items }) => {
    const [edit, setEditing] = useState(false);
    console.log(edit)

    const CancalEditFrom = () => {
        setEditing(false)
    }

    const SaveEditFrom = () => {
        applySnapshot(items, getSnapshot(edit.clone))
        setEditing( false,{
            clone : clone(items)
        })
    }

    const EditForm = () => {
        setEditing({
            clone : clone(items)
        })
    }

    const renderEditable = () => {
        return (
            <div>
                <li>
                    <WishListItemEdit items={edit.clone}/>
                    <button onClick={SaveEditFrom}>save</button>
                    <button onClick={CancalEditFrom}>close</button>
                </li>
            </div>
        )
    }

    return edit ? (
        renderEditable()
    ) : (
        <div>
            <h3>{items.name}</h3>
            <p>{items.price}</p>
            <p>{items.image}</p>
            <button onClick={EditForm}>Edit</button>
            <button onClick={items.remove}>Delete</button>
        </div>
    )

}

export default observer(WishlistItemView)
