
import React, { Component } from "react"
import { observer } from "mobx-react"

import WishListItemEdit from "./WishListItemEdit"

import { WishlistItem } from "../../Models/Wishlist"

class WishListItemEntry extends Component {
    constructor() {
        super()
        this.state = {
            entry: WishlistItem.create({
                name: "",
                price: 0
            })
        }
    }

    render() {
        return (
            <div>
                <WishListItemEdit items={this.state.entry} />
                <button onClick={this.onAdd}>Add</button>
            </div>
        )
    }

    onAdd = () => {
        this.props.items.add(this.state.entry)
        this.setState({
            entry: WishlistItem.create({ name: "", price: 0 })
        })
    }
}

export default observer(WishListItemEntry)

