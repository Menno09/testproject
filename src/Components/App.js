import WishlistView from "./Wishlist/WishlistView";
import '../assets/App.css';

function App ({ wishList }) {
  return (
    <div className="App">
      <h1>Wishlist</h1>
        <WishlistView list={wishList}/>
    </div>
  );
}

export default App;
