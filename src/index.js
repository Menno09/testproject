import React from 'react';
import ReactDOM from 'react-dom';
import './assets/index.css';
import App from './Components/App';
import reportWebVitals from './reportWebVitals';
import {Wishlist} from "./Models/Wishlist";
import { onSnapshot } from "mobx-state-tree"

let initialState = {
    items:  [
        {
            "name": "Max",
            "price": 24.50,
            "image": ""
        },
        {
            "name": "Peter",
            "price": 34.99,
            "image": ""
        },
    ]}


if (localStorage.getItem("wishlistapp")) {
    const json = JSON.parse(localStorage.getItem("wishlistapp"))
    if (Wishlist.is(json)) initialState = json
}

const wishList = Wishlist.create(initialState)

onSnapshot(wishList, snapshot => {
    localStorage.setItem("wishlistapp", JSON.stringify(snapshot))
})

ReactDOM.render(
  <React.StrictMode>
    <App wishList={wishList} />
  </React.StrictMode>,
  document.getElementById('root')
);

reportWebVitals();
