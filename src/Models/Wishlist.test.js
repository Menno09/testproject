import {getSnapshot, onSnapshot, onPatch} from "mobx-state-tree"
import{ WishlistItem, Wishlist } from "./Wishlist"
import {reaction} from "mobx";


it("can create a instance off a model", () => {
    const item = WishlistItem.create({
        "name": "Max",
        "price": 24.99,
    })

    expect(item.price).toBe(24.99)
    expect(item.image).toBe("")
    item.changeName("Jasper")
    expect(item.name).toBe("Jasper")
})

it("can create a wishlist", ()=> {
    const list = Wishlist.create({
        items:  [
            {
                "name": "Max",
                "price": 24.99,
            }
        ]
    })

    expect(list.items.length).toBe(1)
    expect(list.items[0].price).toBe(24.99)
})

it("can add a item to wishlist", ()=> {
    const list = Wishlist.create()
    const stats = []
    onSnapshot(list, snapshot => {
        stats.push(snapshot)
    })
    list.add({
        "name": "Rick",
        "price": 399,
    })

    expect(list.items.length).toBe(1)
    expect(list.items[0].name).toBe("Rick")
    list.items[0].changeName("Jasper")

    expect(getSnapshot(list)).toMatchSnapshot()

    expect(stats).toMatchSnapshot()
})

it("can create a wishlist", ()=> {
    const list = Wishlist.create({
        items:  [
            {
                "name": "Max",
                "price": 24.99,
            }
        ]
    })

    expect(list.items.length).toBe(1)
    expect(list.items[0].price).toBe(24.99)
})

it("can add a item to wishlist - 2 ", ()=> {
    const list = Wishlist.create()
    const patches = []
    onPatch(list, patch => {
        patches.push(patch)
    })
    list.add({
        "name": "Rick",
        "price": 399,
    })

    list.items[0].changeName("Jasper")

    expect(patches).toMatchSnapshot()
})

it("check total price", ()=> {
    const list = Wishlist.create({
        items:  [
            {
                "name": "Max",
                "price": 24,
                "image": ""
            },
            {
                "name": "Peter",
                "price": 34,
                "image": ""
            },
        ]})

    expect(list.totalPrice).toBe(58)

    let changed = 0
    reaction(() => list.totalPrice, () =>changed++)
    list.items[0].changeName("bert")
    expect(changed).toBe(0)
    list.items[0].changePrice(40)
    expect(changed).toBe(1)
})
